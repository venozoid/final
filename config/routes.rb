Rails.application.routes.draw do
  devise_for :users
  resources :admins
  resources :users
  devise_for :admins
  resources :products
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
