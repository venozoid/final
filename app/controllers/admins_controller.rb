class AdminsController < ApplicationController
   before_action :set_user, only: [:show, :edit, :update, :destroy]
  def index
    @admins = Admin.all
  end
  

end